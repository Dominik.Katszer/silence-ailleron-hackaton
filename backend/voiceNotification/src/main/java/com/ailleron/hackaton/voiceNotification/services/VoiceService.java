package com.ailleron.hackaton.voiceNotification.services;

import org.springframework.stereotype.Service;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.File;

@Service
public class VoiceService {

    public void playSound() {
        try {
            File file = new File("C:\\testy\\beep2.wav").getAbsoluteFile();
            System.out.println(file.exists());
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(file);
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
        } catch(Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }
}
