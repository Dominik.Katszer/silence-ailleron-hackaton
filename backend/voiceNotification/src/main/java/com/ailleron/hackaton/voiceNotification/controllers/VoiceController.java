package com.ailleron.hackaton.voiceNotification.controllers;

import com.ailleron.hackaton.voiceNotification.services.VoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins="*")
public class VoiceController {
    @Autowired
    VoiceService voiceService;

    @PostMapping("/notification/sound")
    public void playSound() {
        voiceService.playSound();
    }
}
