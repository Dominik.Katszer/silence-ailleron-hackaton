package com.ailleron.hackaton.voiceNotification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class VoiceNotificationApplication {

	public static void main(String[] args) {
		SpringApplication.run(VoiceNotificationApplication.class, args);
	}
}
