package com.ailleron.hackaton.pushNotification.model;

public class NotificationContent {

    private String title;

    private String body;

    public NotificationContent(String title, String body) {
        this.title = title;
        this.body = body;
    }
}
