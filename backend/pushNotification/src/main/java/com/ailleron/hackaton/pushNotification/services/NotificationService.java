package com.ailleron.hackaton.pushNotification.services;

import java.net.URI;
import java.security.GeneralSecurityException;
import java.security.Security;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ailleron.hackaton.pushNotification.model.Conversation;
import com.ailleron.hackaton.pushNotification.model.NotificationContent;
import com.ailleron.hackaton.pushNotification.model.PushNotification;
import com.google.gson.Gson;

import nl.martijndwars.webpush.Notification;
import nl.martijndwars.webpush.PushService;
import nl.martijndwars.webpush.Subscription;

@Service
public class NotificationService {

    private static final String SUBSCRIPTION_MICROSERVICE_NAME = "SUBSCRIPTION";

    private static final String CONVERSATION_MICROSERVICE_NAME = "CONVERSATION";

    private static final String PUBLIC_KEY = "BP6bJ13sG2OaFoaZmX9I11OEyo9myHI7BL4zpDBo5Y8HFPXHXSXMsWhoN-N14RLXPdmu8CzREbX3mTpQPOye2rs";

    private static final String PRIVATE_KEY = "NcR-Df21eSSt2YGRAcl8ckyTxe4sPbVnYqExrDzYtGk";

    private static final String SUBJECT = "Subject234";

    @Autowired
    private DiscoveryClient client;

    @Autowired
    private RestTemplate restTemplate;

    public void sendPushNotifications(final Long userId, Long min, boolean isFinishConversation)
            throws GeneralSecurityException {
        List<ServiceInstance> instances = client.getInstances(SUBSCRIPTION_MICROSERVICE_NAME);
        Optional<ServiceInstance> first = instances.stream().findFirst();
        URI uri = first.get().getUri();
        if (uri != null) {
            Security.addProvider(new BouncyCastleProvider());
            final Gson gson = new Gson();
            PushService pushService = new PushService(PUBLIC_KEY, PRIVATE_KEY, SUBJECT);
            List<Subscription> subscriptions = callSubscriptionMicroServiceForSubscriptions(userId, uri);
            subscriptions.stream().forEach(sub -> sendNotification(gson, sub, pushService, isFinishConversation));
            if (!isFinishConversation) {
                getDataFromUserService(userId, min);
            }
        }
    }

    private void sendNotification(Gson gson, Subscription sub, PushService pushService, boolean isFinishConversation) {
        try {
            Notification notification = new Notification(sub, prepareSilenceNotification(gson, isFinishConversation));
            pushService.send(notification);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<Subscription> callSubscriptionMicroServiceForSubscriptions(Long userId, URI uri) {
        Subscription[] subscribers = restTemplate.getForObject(uri + "/subscribe/spaces/{id}", Subscription[].class,
                userId);
        return Arrays.asList(subscribers);
    }

    private String prepareSilenceNotification(Gson gson, boolean isFinishConversation) {
        NotificationContent silence;
        if (isFinishConversation) {
            silence = new NotificationContent("SILENCE FINISHED", "You can shout!");
        } else {
            silence = new NotificationContent("SILENCE", "I kill you!");
        }

        PushNotification pushNotification = new PushNotification(silence);
        return gson.toJson(pushNotification);
    }

    private List<Integer> getDataFromUserService(final Long userId, Long min) {
        List<ServiceInstance> list = client.getInstances(CONVERSATION_MICROSERVICE_NAME); // there can be many copies of such microservice
        if (list != null && !list.isEmpty()) {
            URI uri = list.get(0).getUri();
            if (uri != null) {
                Conversation conversation = new Conversation();
                conversation.setEndDate(prepareDate(min));

                restTemplate.postForObject(uri + "/conversation/" + userId, conversation, Long.class);
            }
        }
        return null;
    }

    private Date prepareDate(Long min) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE) + min.intValue());
        return calendar.getTime();
    }

}
