package com.ailleron.hackaton.pushNotification.controllers;

import java.security.GeneralSecurityException;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ailleron.hackaton.pushNotification.services.NotificationService;

@RestController
@CrossOrigin(origins = "*")
public class PushController {

    @Autowired
    private NotificationService notificationService;

    /**
     * Triggers push notification to all users in space where is user with id = userId
     * 
     * @param userId
     * @param min
     * @throws GeneralSecurityException
     */
    @PostMapping("/notification/push/{userId}/{min}")
    public void triggerPushNotification(@PathVariable() @NotNull Long userId, @PathVariable() @NotNull Long min)
            throws GeneralSecurityException {
        notificationService.sendPushNotifications(userId, min, false);
    }

    /**
     * Trigger stop notification to all users in space where is user with id = userId
     * 
     * @param userId
     * @throws GeneralSecurityException
     */
    @PutMapping("/notification/push/{userId}")
    public void triggerStopPushNotification(@PathVariable() @NotNull Long userId) throws GeneralSecurityException {
        notificationService.sendPushNotifications(userId, null, true);
    }
}
