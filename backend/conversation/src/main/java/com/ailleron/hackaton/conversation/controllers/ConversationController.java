package com.ailleron.hackaton.conversation.controllers;

import com.ailleron.hackaton.conversation.model.Conversation;
import com.ailleron.hackaton.conversation.services.ConversationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@CrossOrigin(origins = "*")
public class ConversationController {

    @Autowired
    private ConversationService service;

    @PostMapping("/conversation/{userId}")
    public Long triggerConversation(@RequestBody Conversation conversation, @PathVariable() Long userId) {
        return service.triggerConversation(conversation, userId);
    }

    @GetMapping("/conversation/{userId}")
    public boolean isConversationActive(@PathVariable() Long userId) {
        return service.isConversationActive(userId);
    }

    @GetMapping("/conversation/user/{userId}")
    public boolean isUserConversationActive(@PathVariable() Long userId) {
        return service.isUserConversationActive(userId);
    }

    @PutMapping("/conversation/user/{userId}")
    public void breakActiveConversation(@PathVariable() Long userId) {
        service.breakActiveConversation(userId);
    }

    @PutMapping("/conversation/user/time/{userId}")
    public Date postponeConversation(@PathVariable() Long userId, @RequestParam("time") Long time) {
        return service.postponeUserConversation(userId, time);
    }

}
