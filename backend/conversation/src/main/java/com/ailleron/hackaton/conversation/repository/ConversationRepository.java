package com.ailleron.hackaton.conversation.repository;

import com.ailleron.hackaton.conversation.model.Conversation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ConversationRepository extends JpaRepository<Conversation, Long> {

    List<Conversation> findAllByIsActiveTrue();

    List<Conversation> findAllByUserIdAndIsActiveTrue(@Param("userId") Long userId);

    @Modifying
    @Query("update Conversation set isActive = 'false' where userId in (:usersId)")
    void updateConversationTurnOff(@Param("usersId") List<Long> usersId);

    List<Conversation> findAllBySpaceIdAndIsActiveTrue(@Param("spaceId") Long spaceId);

}
