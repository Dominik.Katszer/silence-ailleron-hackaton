package com.ailleron.hackaton.conversation.services;

import com.ailleron.hackaton.conversation.model.Conversation;
import com.ailleron.hackaton.conversation.repository.ConversationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class ConversationService {
    private ConversationRepository conversationRepository;
    private ConversationCommunicationService communicationService;

    @Autowired
    public ConversationService(ConversationRepository conversationRepository, ConversationCommunicationService conversationCommunicationService) {
        this.conversationRepository = conversationRepository;
        this.communicationService = conversationCommunicationService;
    }

    public Long triggerConversation(Conversation conversation, final Long userId) {
        Long spaceId = communicationService.getSpaceIdByUserId(userId);
        conversation.setUserId(userId);
        conversation.setSpaceId(spaceId);
        conversation.setStartDate(new Date());
        conversation.setActive(true);
        conversation = conversationRepository.save(conversation);
        return conversation.getId();
    }

    public boolean isUserConversationActive(final Long userId) {
        List<Conversation> list = conversationRepository.findAllByUserIdAndIsActiveTrue(userId);
        if (list.isEmpty()) {
            return false;
        }
        return true;
    }

    public boolean isConversationActive(final Long userId) {
        Long spaceId = communicationService.getSpaceIdByUserId(userId);
        List<Conversation> list = conversationRepository.findAllBySpaceIdAndIsActiveTrue(spaceId);
        if (list.isEmpty()) {
            return false;
        }
        return true;
    }

    public void breakActiveConversation(final Long userId) {
        List<Conversation> list = conversationRepository.findAllByUserIdAndIsActiveTrue(userId);
        if (!list.isEmpty()) {
            communicationService.sendInformationAboutConversationsEnd(userId);
            list.stream().forEach(c -> updateConversation(c));
        }
    }

    private void updateConversation(Conversation c) {
        c.setActive(false);
        conversationRepository.save(c);
    }

    public Date postponeUserConversation(final Long userId, final Long time) {
        List<Conversation> list = conversationRepository.findAllByUserIdAndIsActiveTrue(userId);
        if (!list.isEmpty() && list.size() == 1) {
            Conversation conversation = list.get(0);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(conversation.getEndDate());
            calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE) + time.intValue());
            conversationRepository.save(conversation);
            return calendar.getTime();
        }
        return null;
    }
}
