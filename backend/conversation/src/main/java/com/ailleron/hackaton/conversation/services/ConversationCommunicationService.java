package com.ailleron.hackaton.conversation.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@Service
public class ConversationCommunicationService {
    private static final String PUSH_NOTIFICATION_MICROSERVICE_NAME = "PUSHNOTIFICATION";
    private static final String USER_MICROSERVICE_NAME = "USERSERVICE";

    private DiscoveryClient client;

    private RestTemplate restTemplate;

    @Autowired
    public ConversationCommunicationService(DiscoveryClient client, RestTemplate restTemplate) {
        this.client = client;
        this.restTemplate = restTemplate;
    }

    public void sendInformationAboutConversationsEnd(final Long userId) {
        List<ServiceInstance> list = client.getInstances(PUSH_NOTIFICATION_MICROSERVICE_NAME); // there can be many copies of such microservice
        if (list != null && !list.isEmpty()) {
            URI uri = list.get(0).getUri();
            if (uri != null) {
                restTemplate.put(uri + "/notification/push/" + userId, null);
            }
        }
    }

    public Long getSpaceIdByUserId(final Long userId) {
        List<ServiceInstance> list = client.getInstances(USER_MICROSERVICE_NAME); // there can be many copies of such microservice
        if (list != null && !list.isEmpty()) {
            URI uri = list.get(0).getUri();
            if (uri != null) {
                return restTemplate.getForObject(uri + "/users/{id}/space", Long.class, userId);
            }
        }
        return null;
    }


}
