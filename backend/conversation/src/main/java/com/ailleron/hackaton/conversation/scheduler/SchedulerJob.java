package com.ailleron.hackaton.conversation.scheduler;


import com.ailleron.hackaton.conversation.model.Conversation;
import com.ailleron.hackaton.conversation.repository.ConversationRepository;
import com.ailleron.hackaton.conversation.services.ConversationCommunicationService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class SchedulerJob {

    private ConversationRepository repository;
    private ConversationCommunicationService conversationCommunicationService;

    public SchedulerJob(ConversationRepository repository, ConversationCommunicationService conversationCommunicationService) {
        this.repository = repository;
        this.conversationCommunicationService = conversationCommunicationService;
    }

    @Scheduled(fixedRate = 60000)
    @Transactional
    public void job() {
        List<Conversation> activeConversations = repository.findAllByIsActiveTrue();
        List<Conversation> stopConversation = new ArrayList<>();
        for (Conversation c : activeConversations) {
            if (c.getEndDate().compareTo(new Date()) < 0) {
                stopConversation.add(c);
            }
        }

        System.out.println(new Date() + " - " + stopConversation.size() + " conversations found to stop.");
        List<Long> usersId = new ArrayList<>();
        stopConversation.stream().forEach(c -> usersId.add(c.getUserId()));
        repository.updateConversationTurnOff(usersId);
        for (Long userId : usersId) {
            conversationCommunicationService.sendInformationAboutConversationsEnd(userId);
        }
    }
}
