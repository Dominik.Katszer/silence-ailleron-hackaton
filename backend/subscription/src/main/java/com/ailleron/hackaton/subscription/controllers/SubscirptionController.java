package com.ailleron.hackaton.subscription.controllers;

import com.ailleron.hackaton.subscription.services.SubscribeService;
import nl.martijndwars.webpush.Subscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class SubscirptionController {

    @Autowired
    private SubscribeService subscribeService;

    @PostMapping("/subscribe/{userId}")
    public void subscribe(@RequestBody Subscription subscription, @PathVariable() Long userId) {
        subscribeService.subscribe(subscription, userId);
    }

    @GetMapping("/subscribe/spaces/{userId}")
    public List<Subscription> getSubscribers(@PathVariable() @NotNull Long userId) {
        return subscribeService.getAllSubscribers(userId);
    }
}
