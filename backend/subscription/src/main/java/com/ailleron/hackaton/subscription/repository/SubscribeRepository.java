package com.ailleron.hackaton.subscription.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ailleron.hackaton.subscription.model.UserBrowser;

public interface SubscribeRepository extends JpaRepository<UserBrowser, Long> {

    @Query("Select ub from UserBrowser ub where userId in (:ids)")
    List<UserBrowser> getUserBrowsersByUserId(@Param("ids") List<Integer> ids);

    List<UserBrowser> getUserBrowserByUrl(@Param("url") String url);
}
