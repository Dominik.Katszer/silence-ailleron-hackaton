package com.ailleron.hackaton.subscription.services;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ailleron.hackaton.subscription.model.UserBrowser;
import com.ailleron.hackaton.subscription.repository.SubscribeRepository;

import nl.martijndwars.webpush.Subscription;

@Service
public class SubscribeService {

    private SubscribeRepository subscribeRepository;

    private RestTemplate restTemplate;

    private DiscoveryClient client;

    private static final String USER_MICROSERVICE_NAME = "USERSERVICE";

    @Autowired
    public SubscribeService(SubscribeRepository subscribeRepository, RestTemplate restTemplate,
            DiscoveryClient client) {
        this.subscribeRepository = subscribeRepository;
        this.restTemplate = restTemplate;
        this.client = client;
    }

    public void subscribe(final Subscription subscription, final Long userId) {
        UserBrowser userBrowser = prepareUserBrowserObject(subscription, userId);
        List<UserBrowser> userBrowserByUrl = subscribeRepository.getUserBrowserByUrl(userBrowser.getUrl());
        if (userBrowserByUrl.isEmpty()) {
            subscribeRepository.save(userBrowser);
        }
    }

    private UserBrowser prepareUserBrowserObject(Subscription subscription, Long userId) {
        UserBrowser userBrowser = new UserBrowser();
        userBrowser.setUrl(subscription.endpoint);
        userBrowser.setAuth(subscription.keys.auth);
        userBrowser.setP256(subscription.keys.p256dh);
        userBrowser.setUserId(userId.intValue());
        return userBrowser;
    }

    public List<Subscription> getAllSubscribers(final Long userId) {
        List<Integer> userIds = getDataFromUserService(userId);
        List<UserBrowser> userBrowsers = subscribeRepository.getUserBrowsersByUserId(userIds);
        return mapUserBrowsersToSubscribers(userBrowsers);
    }

    private List<Subscription> mapUserBrowsersToSubscribers(List<UserBrowser> userBrowsers) {
        List<Subscription> subscribers = new ArrayList<>();
        userBrowsers.stream().forEach(u -> prepareSubscribers(subscribers, u));
        return subscribers;
    }

    private void prepareSubscribers(List<Subscription> subscribers, UserBrowser u) {
        Subscription s = new Subscription();
        s.endpoint = u.getUrl();
        s.keys = s.new Keys();
        s.keys.auth = u.getAuth();
        s.keys.p256dh = u.getP256();
        subscribers.add(s);
    }

    private List<Integer> getDataFromUserService(final Long userId) {
        List<ServiceInstance> list = client.getInstances(USER_MICROSERVICE_NAME); // there can be many copies of such microservice
        if (list != null && !list.isEmpty()) {
            URI uri = list.get(0).getUri();
            if (uri != null) {
                return restTemplate.getForObject(uri + "/users/space/{id}", List.class, userId);
            }
        }
        return null;
    }

}
