package com.ailleron.hackaton.user.repository;

import com.ailleron.hackaton.user.model.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import java.util.List;
import java.util.Optional;

public interface UserDetailsRepository extends JpaRepository<UserDetails, Long> {
    Optional<UserDetails> findByLogin(@Param("login") String login);
    Optional<UserDetails> findByRole(@Param("role") String role);

    @Query("select id from UserDetails where spaceId = (select spaceId from UserDetails where id = :userId)")
    List<Long> getUsersIdBySpaceId(@Param("userId") Long userId);

   Optional<UserDetails> getUserByKey(@Param("key") String key);

   Optional<UserDetails> getUserByLogin(@Param("login") String login);

   @Query("select spaceId from UserDetails where id = :userId")
   Long getSpaceIdByUserId(@Param("userId") Long userId);

}
