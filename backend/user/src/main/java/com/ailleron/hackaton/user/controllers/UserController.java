package com.ailleron.hackaton.user.controllers;

import java.util.List;

import javax.mail.MessagingException;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ailleron.hackaton.user.exceptions.PasswordOrLoginIncorrect;
import com.ailleron.hackaton.user.exceptions.SessionExpired;
import com.ailleron.hackaton.user.exceptions.UserExistException;
import com.ailleron.hackaton.user.exceptions.UserNotFoundException;
import com.ailleron.hackaton.user.model.UserDetails;
import com.ailleron.hackaton.user.services.UserService;

@RestController
@CrossOrigin(origins = "*")
public class UserController {

    @Autowired
    private UserService service;

    @PostMapping("/users/login")
    public Long loginUser(@RequestBody @Valid UserDetails userDetails)
            throws PasswordOrLoginIncorrect, UserNotFoundException, SessionExpired {
        return service.loginUser(userDetails);
    }

    @PostMapping("/users/session")
    public void updateUserSession(@RequestBody @NotNull Long userId) throws SessionExpired {
        service.updateUserSession(userId);
    }

    @PostMapping("/users/active/{userId}")
    public boolean checkIfActive(@PathVariable @NotNull Long userId) {
        try {
            service.updateUserSession(userId);
            return true;
        } catch (UnsupportedOperationException | SessionExpired e) {
            return false;
        }
    }

    @PostMapping("/users/register")
    public void registerAccount(@RequestBody @Valid UserDetails userDetails)
            throws MessagingException, UserExistException {
        service.registerAccount(userDetails);
    }

    @GetMapping("/users/space/{userId}")
    public List<Long> getUsersBySpaceId(@PathVariable() @NotNull Long userId) {
        return service.getUsersIdFromSpace(userId);
    }

    @GetMapping("/users/{userId}/space")
    public Long getSpaceIdByUserId(@PathVariable() @NotNull Long userId) {
        return service.getSpaceIdByUserId(userId);
    }

    @GetMapping("/users/confirm/{key}")
    public void confirmAccount(@PathVariable() @NotNull String key) throws UserNotFoundException, MessagingException {
        service.confirmAccount(key);
    }
}
