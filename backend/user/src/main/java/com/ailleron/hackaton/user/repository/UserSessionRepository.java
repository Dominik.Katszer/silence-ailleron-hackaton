package com.ailleron.hackaton.user.repository;

import com.ailleron.hackaton.user.model.UserSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserSessionRepository extends JpaRepository<UserSession, Long> {
    Optional<UserSession> findByUserIdAndIsActive(@Param("userId") Long userId, @Param("isActive") Boolean isActive);

    @Modifying
    @Query("update UserSession set isActive = 'false' where id = :userId")
    void updateSessionSetNonActive(@Param("userId") Long userId);
}
