package com.ailleron.hackaton.user.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class EmailProperties {

    private static Properties properties = null;

    public Properties getEmailProperties() {
        synchronized (this) {
            if (properties == null) {
                System.out.println("Creating properties for email configuration");
                properties = new Properties();
                prepareEmailProperties();

            } else {
                System.out.println("Properties for email configuration created.");
            }
            return properties;
        }
    }

    private void prepareEmailProperties() {
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.auth", true);
        properties.put("mail.smtp.port", 587);
        properties.put("mail.smtp.starttls.enable", true);
        properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
    }
}
