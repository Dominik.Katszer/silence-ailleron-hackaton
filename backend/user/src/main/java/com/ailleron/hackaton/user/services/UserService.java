package com.ailleron.hackaton.user.services;

import com.ailleron.hackaton.user.email.EmailSender;
import com.ailleron.hackaton.user.exceptions.PasswordOrLoginIncorrect;
import com.ailleron.hackaton.user.exceptions.SessionExpired;
import com.ailleron.hackaton.user.exceptions.UserExistException;
import com.ailleron.hackaton.user.exceptions.UserNotFoundException;
import com.ailleron.hackaton.user.model.UserDetails;
import com.ailleron.hackaton.user.model.UserSession;
import com.ailleron.hackaton.user.repository.UserDetailsRepository;
import com.ailleron.hackaton.user.repository.UserSessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {

    private static final Integer SESSION_TIME = 15;

    @Autowired
    EmailSender sender;

    private UserSessionRepository userSessionRepository;
    private UserDetailsRepository userDetailsRepository;

    @Autowired
    public UserService(UserSessionRepository userSessionRepository, UserDetailsRepository userDetailsRepository) {
        this.userSessionRepository = userSessionRepository;
        this.userDetailsRepository = userDetailsRepository;
    }

    @Transactional
    public Long loginUser(final UserDetails user) throws UserNotFoundException, PasswordOrLoginIncorrect, SessionExpired {
        UserDetails existingUser = getUserAccount(user);
        createSession(existingUser.getId());
        return existingUser.getId();
    }

    private UserDetails getUserAccount(final UserDetails user) throws UserNotFoundException, PasswordOrLoginIncorrect {
        Optional<UserDetails> optionalUser = userDetailsRepository.findByLogin(user.getLogin());
        if (!optionalUser.isPresent()) {
            throw new UserNotFoundException("User not found");
        }
        UserDetails existingUser = optionalUser.get();
        if (!existingUser.isConfirmed()) {
            throw new UserNotFoundException("User not found");
        }
        if (!validatePassword(user, existingUser)) {
            throw new PasswordOrLoginIncorrect("Password or login incorrect");
        }
        return existingUser;
    }

    private boolean validatePassword(UserDetails user, UserDetails existingUser) throws UserNotFoundException {
        return user.getPassword().equals(existingUser.getPassword());
    }

    private void createSession(final Long userId) {
        userSessionRepository.updateSessionSetNonActive(userId);
        UserSession userSession = new UserSession(userId, prepareExpirationDate().getTime(), true);
        userSessionRepository.save(userSession);
    }

    public void updateUserSession(final Long userId) throws SessionExpired {
        UserSession userSession = null;
        Optional<UserSession> existingSession = userSessionRepository.findByUserIdAndIsActive(userId, true);
        if (!existingSession.isPresent()) {
            throw new UnsupportedOperationException("No session found!");
        } else {
            userSession = existingSession.get();
            if (userSession.getExpirationDate().compareTo(new Date()) < 0) {
                userSession.setActive(false);
                userSessionRepository.save(userSession);
                throw new SessionExpired("Your session expired!");
            }
            userSession.setExpirationDate(prepareExpirationDate().getTime());
        }
        userSessionRepository.save(userSession);
    }

    private Calendar prepareExpirationDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE) + SESSION_TIME);
        return cal;
    }

    @Transactional
    public void registerAccount(final UserDetails details) throws MessagingException, UserExistException {
        if (userDetailsRepository.getUserByLogin(details.getLogin()).isPresent()) {
            throw new UserExistException("User Exist!");
        }
        details.setKey(generateUuid());
        details.setRole("USER");
        details.setConfirmed(false);
        userDetailsRepository.save(details);
        Optional<UserDetails> optionalPM = userDetailsRepository.findByRole("PM");
        if (!optionalPM.isPresent()) {
            throw new UnsupportedOperationException();
        }
        sender.send(optionalPM.get().getEmail(), "User needs your confirmation!", "http://localhost:8011/users/confirm/" + details.getKey());
    }

    private String generateUuid() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    public List<Long> getUsersIdFromSpace(final Long userId) {
        return userDetailsRepository.getUsersIdBySpaceId(userId);
    }

    public Long getSpaceIdByUserId(final Long userId) {
        return userDetailsRepository.getSpaceIdByUserId(userId);
    }

    public void confirmAccount(final String key) throws UserNotFoundException, MessagingException {
        Optional<UserDetails> user = userDetailsRepository.getUserByKey(key);
        if (!user.isPresent()) {
            throw new UserNotFoundException("USer not found");
        }
        UserDetails userDetails = user.get();
        if (!userDetails.isConfirmed()) {
            userDetails.setConfirmed(true);
        }
        userDetailsRepository.save(userDetails);
        sender.send(userDetails.getEmail(), "Your account is ready!", "you can login now!");
    }
}
