package com.ailleron.hackaton.user.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;
import javax.validation.constraints.Size;

@Entity
@TableGenerator(name = "userIdGenerator", initialValue = 1)
public class UserDetails{
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "userIdGenerator")
    private Long id;
    private String name;
    private String surname;
    private String email;
    private String role;
    private String key;
    private Long spaceId;

    @Size(min = 5, max = 20)
    private String login;
    @Size(min = 8, max = 20)
    private String password;
    private boolean isConfirmed;

    public UserDetails() {
    }

    public UserDetails(Long id, String name, String surname, String email, String role, String login, String password, String key, Long spaceId) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.role = role;
        this.key = key;
        this.spaceId = spaceId;
        this.login = login;
        this.password = password;
    }

    public Long getSpaceId() {
        return spaceId;
    }

    public void setSpaceId(Long spaceId) {
        this.spaceId = spaceId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(boolean confirmed) {
        isConfirmed = confirmed;
    }
}
