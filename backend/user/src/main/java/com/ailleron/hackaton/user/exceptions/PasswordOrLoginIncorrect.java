package com.ailleron.hackaton.user.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class PasswordOrLoginIncorrect extends Exception {
    public PasswordOrLoginIncorrect(String message) {
        super(message);
    }
}
