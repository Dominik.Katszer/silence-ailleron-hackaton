package com.ailleron.hackaton.user.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
public class SessionExpired extends Exception {
    public SessionExpired(String message) {
        super(message);
    }
}
