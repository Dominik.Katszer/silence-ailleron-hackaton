import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ConversationComponent} from './app/component/conversation/conversation.component';
import {LoginFormComponent} from './app/component/login-form/login-form.component';
import {RegistrationComponent} from './app/component/registration/registration.component';
import {AuthGuardService} from './app/service/auth-guard.service';
import {DoneComponent} from "./app/component/registration/done/done.component";


const routes: Routes = [
  {path: '', redirectTo: '/conversation', pathMatch: 'full'},
  {path: 'login', component: LoginFormComponent},
  {path: 'conversation', component: ConversationComponent, canActivate: [AuthGuardService]},
  {path: 'registration', component: RegistrationComponent},
  {path: 'registration-complete', component: DoneComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
