import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'; // <-- NgModel lives here
import {MatInputModule} from '@angular/material/input';
import {MatStepperModule} from '@angular/material/stepper';
import {
  MatButtonModule, MatCardModule, MatIconModule, MatListModule, MatRadioModule, MatSidenavModule,
  MatToolbarModule, MatSnackBar, MatSnackBarModule
} from '@angular/material';
import {MatSelectModule} from '@angular/material/select';
import {FlexLayoutModule} from '@angular/flex-layout';

import {AppComponent} from './app.component';
import {LoginFormComponent} from './component/login-form/login-form.component';
import {ConversationComponent} from './component/conversation/conversation.component';
import {AppRoutingModule} from '../app-routing.module';
import {ServiceWorkerModule, SwUpdate} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { RegistrationComponent } from './component/registration/registration.component';
import { OfficeFormComponent } from './component/registration/office-form/office-form.component';
import { FloorplanComponent } from './component/registration/floorplan/floorplan.component';
import { AccountFormComponent } from './component/registration/account-form/account-form.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { DoneComponent } from './component/registration/done/done.component';
import { AppNavbarComponent } from './component/app-navbar/app-navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    ConversationComponent,
    RegistrationComponent,
    OfficeFormComponent,
    FloorplanComponent,
    AccountFormComponent,
    DoneComponent,
    AppNavbarComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatListModule,
    MatInputModule,
    MatRadioModule,
    MatSidenavModule,
    MatToolbarModule,
    MatSelectModule,
    MatSnackBarModule,
    FlexLayoutModule,
    AppRoutingModule,
    MatStepperModule,
    MatAutocompleteModule,
    HttpClientModule,
    ServiceWorkerModule.register('/ngsw-worker.js', {enabled: environment.production})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(update: SwUpdate, snackbar: MatSnackBar, private http: HttpClient) {
    update.available.subscribe(update => {
      console.log(update);
      const bar = snackbar.open('Update is available', 'Reload');
      bar.onAction().subscribe(() => {
        window.location.reload();
      });
    });
  }
}
