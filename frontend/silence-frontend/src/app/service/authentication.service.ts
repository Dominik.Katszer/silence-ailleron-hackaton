import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LoginModel} from '../component/login-form/login-model';
import {Observable} from 'rxjs';
import {ENDPOINT_URL} from '../SilenceUtil';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) {
  }

  loginUser(model: LoginModel): Observable<any> {
    const headers = (new HttpHeaders())
      .set('Content-type', 'application/json')
      .set('Access-Control-Allow-Method', 'POST');
    return this.http.post(ENDPOINT_URL + ':8011/users/login', JSON.stringify(model), {headers: headers});
  }

  checkIfUserActive(userId: string): Observable<boolean> {
    const headers = (new HttpHeaders())
      .set('Content-type', 'application/json')
      .set('Access-Control-Allow-Method', 'POST');
    return this.http.post<boolean>(ENDPOINT_URL + ':8011/users/active/' + userId, {headers: headers});
  }
}
