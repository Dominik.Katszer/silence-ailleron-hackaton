import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthenticationService} from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router, private authenticationService: AuthenticationService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    let item = localStorage.getItem('user');
    console.log('AuthGuard call: ' + item);
    if (item === null || item === undefined) {
      console.log('AuthGuard not logged yet');
      this.router.navigate(['/login']);
      return false;
    }
    if (this.authenticationService.checkIfUserActive(localStorage.getItem('user'))) {
      return true;
    } else {
      // Navigate to the login page
      this.router.navigate(['/login']);
      return false;
    }
  }

}
