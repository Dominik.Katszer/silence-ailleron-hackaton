import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ENDPOINT_URL} from '../SilenceUtil';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private http: HttpClient) {
  }

  sendSubscription(pushSubscription, userId: string) {
    const headers = (new HttpHeaders())
      .set('Content-type', 'application/json')
      .set('Access-Control-Allow-Method', 'POST');
    this.http.post(ENDPOINT_URL + ':8012/subscribe/' + userId, pushSubscription.toJSON(), {headers: headers})
      .subscribe(res => {
          console.log('[App] Add subscriber request answer', res);
        },
        err => {
          console.log('[App] Add subscriber request failed', err);
        });
  }

  triggerSilence(userId: string, conversationTime: number) {
    const headers = (new HttpHeaders())
      .set('Content-type', 'application/json')
      .set('Access-Control-Allow-Method', 'POST');
    this.http.post(ENDPOINT_URL + ':8013/notification/push/' + userId + '/' + conversationTime, {headers: headers}).subscribe(res => {
        console.log('[App] Add silence request answer', res);
      },
      err => {
        console.log('[App] Add silence request failed', err);
      });
  }

  triggerSound() {
    const headers = (new HttpHeaders())
      .set('Content-type', 'application/json')
      .set('Access-Control-Allow-Method', 'POST');
    this.http.post(ENDPOINT_URL + ':8015/notification/sound', {headers: headers}).subscribe(res => {
        console.log('[App] triggerSound answer', res);
      },
      err => {
        console.log('[App] triggerSound failed', err);
      });
  }

}
