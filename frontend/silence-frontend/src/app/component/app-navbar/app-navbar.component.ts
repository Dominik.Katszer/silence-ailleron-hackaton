import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './app-navbar.component.html',
  styleUrls: ['./app-navbar.component.scss']
})
export class AppNavbarComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  isLoggedIn(): boolean {
    let item = localStorage.getItem('user');
    if (item === null || item === undefined) {
      return false;
    }
    return true;
  }

  logout() {
    localStorage.clear();
    //redirect to login page
    this.router.navigate(['/login']);
  }
}
