import {Component, OnInit} from '@angular/core';
import {SwPush} from '@angular/service-worker';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {ENDPOINT_URL, PUBLIC_KEY} from '../../SilenceUtil';
import {NotificationService} from '../../service/notification.service';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.scss']
})
export class ConversationComponent implements OnInit {

  conversationTime: number = 1;
  times: number[] = [1, 5, 10, 15, 30, 45, 60];
  isAnyConversationOnSpace : boolean = false;
  isMyConversationOnSpace: boolean = false;

  constructor(private push: SwPush, private http: HttpClient, private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.push.requestSubscription({serverPublicKey: PUBLIC_KEY}).then(pushSubscription => {
      console.log(pushSubscription.toJSON());
      const userId: string = localStorage.getItem('user');
      this.notificationService.sendSubscription(pushSubscription, userId);
    });

    this.checkMySpace();
    this.checkMyConversation();

    this.push.messages.subscribe(msg => {
      let json = JSON.stringify(msg);
      let parse = JSON.parse(json);
      let tittle = parse.notification.title;
      if (tittle === 'SILENCE') {
        this.isAnyConversationOnSpace = true;
      } else {
        this.isAnyConversationOnSpace = false;
        this.isMyConversationOnSpace = false;
      }
    });
  }

  triggerSilent(): void {
    console.log('silence triggered!');
    console.log(this.conversationTime);
    const userId: string = localStorage.getItem('user');
    this.isMyConversationOnSpace = true;
    this.isAnyConversationOnSpace = true;
    this.notificationService.triggerSilence(userId, this.conversationTime);
    this.notificationService.triggerSound();
  }

  stopYourConversation() {
    let userId = localStorage.getItem('user');
    this.http.put(ENDPOINT_URL + ':8014/conversation/user/' + userId, null)
      .subscribe(_ => this.isMyConversationOnSpace = false);
  }

  checkMySpace() {
    let userId = localStorage.getItem('user');
    this.http.get<boolean>(ENDPOINT_URL + ':8014/conversation/' + userId)
      .subscribe(res => this.isAnyConversationOnSpace = res);
  }

  checkMyConversation() {
    let userId = localStorage.getItem('user');
    this.http.get<boolean>(ENDPOINT_URL + ':8014/conversation/user/' + userId)
      .subscribe(res => this.isMyConversationOnSpace = res);
  }

  postponeSilent() {
    let userId = localStorage.getItem('user');
    const params = new HttpParams()
      .set('time', this.conversationTime.toString());

    this.http.put('http://localhost:8014/conversation/user/time/'+userId,null, {params})
      .subscribe( res => alert(res));
  }
}
