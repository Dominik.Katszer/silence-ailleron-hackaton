import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {ENDPOINT_URL} from '../../SilenceUtil';

/**
 * @title Stepper overview
 */
@Component({
  selector: 'registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit {
  officeFormGroup: FormGroup;
  accountFormGroup: FormGroup;
  floorplanFormGroup: FormGroup;

  constructor(private _formBuilder: FormBuilder, private http: HttpClient, private router: Router) {
  }

  ngOnInit() {
    this.officeFormGroup = this._formBuilder.group({
      city: ['', Validators.required],
      office: ['', Validators.required],
      floor: ['', Validators.required]
    });
    this.floorplanFormGroup = this._formBuilder.group({
      spaceId: ['', Validators.required]
    });
    this.accountFormGroup = this._formBuilder.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      email: ['', Validators.required],
      login: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  applyStepper() {
    const headers = (new HttpHeaders())
      .set('Content-type', 'application/json')
      .set('Access-Control-Allow-Method', 'POST');
    debugger;
    let body = {
      name: this.accountFormGroup.value.name,
      surname: this.accountFormGroup.value.surname,
      email: this.accountFormGroup.value.email,
      spaceId: this.floorplanFormGroup.value.spaceId,
      login: this.accountFormGroup.value.login,
      password: this.accountFormGroup.value.password
    };
    // alert(JSON.stringify(body));
    console.log(JSON.stringify(body));
    this.http.post(ENDPOINT_URL + ':8011/users/register', JSON.stringify(body), {headers: headers}).subscribe(result => console.log(result), error1 => console.log(error1));
    this.router.navigate(['registration-complete']);
  }
}
