import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'app-office-form',
  templateUrl: './office-form.component.html',
  styleUrls: ['./office-form.component.scss']
})
export class OfficeFormComponent implements OnInit {

  @Input()
  formGroup: FormGroup;
  cities: string[] = ['Kraków', 'Rzeszów', 'Bielsko-Biała'];
  offices: string[] = ['Życzkowskiego 19', 'Życzkowskiego 20'];
  floors: string[] = ['1', '2'];

  constructor() { }

  ngOnInit() {
  }
}
