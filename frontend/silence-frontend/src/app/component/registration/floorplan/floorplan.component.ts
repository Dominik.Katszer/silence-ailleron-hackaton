import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import * as $ from 'jquery';
import {stringify} from 'querystring';

@Component({
  selector: 'app-floorplan',
  templateUrl: './floorplan.component.html',
  styleUrls: ['./floorplan.component.scss']
})
export class FloorplanComponent implements OnInit {

  @Input()
  formGroup: FormGroup;

  spaceId: any = 1;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    var reader = new FileReader();
    reader.onload = function (progressEvent) {
      document.getElementById('drawing').innerHTML = this.result;
      $('.space').click(function (event) {
        $('.space').not($(this)).css({
          'stroke-width': '1',
          'stroke': 'black',
          'fill': 'yellow'
        });
        $(this).css({
          'stroke-width': '6',
          'stroke': 'red',
          'fill': 'pink'
        });
        this.spaceId = event.target.id;
        (<HTMLInputElement>document.getElementById('spaceIdInput')).value = event.target.id;

        // this.formGroup.patchValue({
        //   'spaceId': event.target.id
        // });

      });
    };
    this.http.get('assets/biuro-krk-zyczkowskiego-19-1.svg', {responseType: 'blob'}).subscribe(data => reader.readAsText(data));
  }
}
