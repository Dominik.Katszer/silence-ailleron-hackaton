import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {LoginModel} from './login-model';
import {AuthenticationService} from '../../service/authentication.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  hidePassword: boolean;
  model: LoginModel = {
    login: '',
    password: ''
  };

  constructor(private router: Router, private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
  }

  login() {
    console.log(this.model);
    this.authenticationService.loginUser(this.model).subscribe(res => {
        console.log('[App] login user request answer', res);
        localStorage.setItem('user', res.toString());
        this.router.navigate(['/conversation']);
      },
      err => {
        console.log('[App] login user request failed', err);
        console.log('Login failed');
      });
  }

  redirectToRegister() {
    this.router.navigate(['registration']);
  }
}
